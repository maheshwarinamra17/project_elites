AWS_CRED = {
    region: 'us-east-1',
    credentials: Aws::Credentials.new(SECRETS_CONFIG['aws_access_key_id'],
                                      SECRETS_CONFIG['aws_secret_access_key']),
}
Aws.config.update(AWS_CRED)
S3_BUCKET = Aws::S3::Resource.new.bucket(APP_CONFIG['img_bucket'])