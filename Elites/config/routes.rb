Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  post 'api_authenticate', to: 'authentication#authenticate_user'
  post 'api_signup', to: 'user#create_a_new_user'
  post 'api_secondary_info', to: 'user#create_secondary_info'
  post 'api_add_subjects', to: 'dropdown#add_subjects'
  post 'api_add_time_slots', to: 'dropdown#add_time_slots'
  post 'api_add_student_standards', to: 'dropdown#add_student_standards'
  post 'api_add_course', to: 'course#add_a_course'
  get 'api_get_courses_by_user', to: 'course#get_courses_by_user'
  get 'api_delete_a_course_by_user', to: 'course#delete_a_course_by_user'
  post 'api_schedule_a_booking', to: 'schedule#schedule_a_booking'
  post 'api_cancel_a_booking', to: 'schedule#cancel_a_booking'
  get 'api_search_courses', to: 'course#search_courses'
  post 'api_distance_matrix', to: 'common_utils#get_distance_matrix'
  post 'api_save_img', to: 'dropdown#upload_img_to_s3'
  post 'api_update_payment_info', to: 'schedule#update_payment_info'
  post 'api_test_api', to: 'dropdown#test_api'

  root :to => "page#home"
  match '/login',    to: 'page#login_user', via: :get
  match '/dashboard',    to: 'page#dashboard', via: :get
  match '/schedule',    to: 'page#schedule', via: :get
  match '/signup',    to: 'page#signup', via: :get
  match '/secondary_info',    to: 'page#secondary_info', via: :get
  match '/students',    to: 'page#students', via: :get
  match '/tnc',    to: 'page#tnc', via: :get
  match '/privacy_policy',    to: 'page#privacy_policy', via: :get
  match '/pricing',    to: 'page#pricing', via: :get
  match '/contact_us',    to: 'page#contact_us', via: :get

end
