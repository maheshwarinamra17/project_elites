class PaymentsHelper

  def generate_payment_details course, plan
    result = {}
    am_res = get_payble_amount plan, course['c_rates']
    if am_res.present?
      result.merge!(am_res)
      result['course_tax_per'] = APP_CONFIG['payments_data']['service_tax']
      result['course_tax_val'] = am_res['course_total']*APP_CONFIG['payments_data']['service_tax'] / 100.0
      result['course_amount'] = result['course_tax_val'] + am_res['course_total']
    end
    return result
  end

  def get_payble_amount plan, course_rates
    result = {}
    plan = plan.to_i
    course_rates = JSON.parse(course_rates)
    if plan < APP_CONFIG['course_plan']['1_month'].to_f
      result['course_total'] = course_rates['rph'].to_f
      result['course_rate'] = course_rates['rph'].to_f
    else
      result['course_total'] = (course_rates['rpm'].to_f * plan) / APP_CONFIG['course_plan']['1_month'].to_i
      result['course_rate'] = course_rates['rpm'].to_f
    end
    return result
  end

end