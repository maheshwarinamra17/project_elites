class CommonUtils

  def parse_distance_matrix distance_matrix, dest_location_hash
    result = {}
    dest_array = distance_matrix['destination_addresses']
    distance_array = distance_matrix['rows'][0]['elements'].map{|x| x["distance"]["text"]}
    index = 0
    dest_location_hash.each do |dest_id|
      result[dest_id] = {}
      result[dest_id]["addr"] = dest_array [index]
      result[dest_id]["dist"] = distance_array[index]
      index+=1
    end
    return result
  end

  def get_location_data origin_location, dest_location_hash
    result = {}
    url = URI.parse(APP_CONFIG['distance_matrix'])
    dest_location_array = dest_location_hash.values
    dest_location = dest_location_array.join("|")
    parameters = {
      :units => "metric",
      :origins => origin_location,
      :destinations => dest_location,
      :key => Rails.application.secrets.google_maps_KEY
    }
    begin
      response = HTTParty.post(url, :query => parameters)
      if response.present?
        result = parse_distance_matrix response, dest_location_hash
      end
      result
    rescue Exception => e
      result = nil
    end
  end

  def split_base64(uri_str)
    if uri_str.match(%r{^data:(.*?);(.*?),(.*)$})
      uri = Hash.new
      uri[:type] = $1 # "image/gif"
      uri[:encoder] = $2 # "base64"
      uri[:data] = $3 # data string
      uri[:extension] = $1.split('/')[1] # "gif"
      return uri
    else
      return nil
    end
  end

  def base64_string_to_file image_data_64, file_name
    image_data = split_base64(image_data_64)
    image_data_string = image_data[:data]
    image_data_binary = Base64.decode64(image_data_string)
    temp_img_file = Tempfile.new("")
    temp_img_file.binmode
    temp_img_file << image_data_binary
    temp_img_file.rewind
    file_name = file_name + "." + image_data[:type].split("/")[1]
    file_loc = Rails.root.join('public/uploads', file_name)
    File.open(file_loc, 'wb') do |file|
      file.write(temp_img_file.read)
    end
    return file_loc
  end

  def upload_img_s3 key, img_data
    salt = SecureRandom.urlsafe_base64(32)
    img_name = "#{key}_#{salt}"
    file_name = base64_string_to_file(img_data, img_name)
    begin
      s3_obj = S3_BUCKET.object(img_name)
      s3_obj.upload_file(file_name)
      s3_obj.acl.put({ acl: "public-read" })
      File.delete(file_name) if File.exist?(file_name)
      result = "#{APP_CONFIG['s3_url']}#{APP_CONFIG['img_bucket']}/#{img_name}"
    rescue Exception => e
      result = nil
    end
  end

  def send_sms mobile, text
    result = {}
    url = URI.parse(APP_CONFIG['sms_api'])
    parameters = {
        :user => Rails.application.secrets.sms_username,
        :pass => Rails.application.secrets.sms_pass,
        :sender => Rails.application.secrets.sms_sender,
        :phone => mobile,
        :text => text,
        :priority => "ndnd",
        :stype => "normal"
    }
    begin
      response = HTTParty.post(url, :query => parameters)
      if response.present?
        result['data'] = response.to_s
      end
      result
    rescue Exception => e
      result = nil
    end
  end


end