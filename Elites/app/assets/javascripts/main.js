/**
 * Created by namra on 18/05/17.
 * TODO - Add No results div
 * TODO - Add Upload image
 */



var EliteJS = EliteJS || {};

EliteJS.mother_js = function () {

    //API
    var API_SIGNUP = "/api_signup";
    var API_LOGIN = "/api_authenticate";
    var API_SECONDARY_INFO = "/api_secondary_info";
    var API_ADD_COURSE = "/api_add_course";
    var API_COURSES_USER = "/api_get_courses_by_user";
    var API_SEARCH_COURSES = "/api_search_courses";
    var API_DISTANCE_MATRIX = "/api_distance_matrix";
    var API_IMAGE_UPLOAD = "/api_save_img";
    var API_SCHEDULE_A_BOOKING = "/api_schedule_a_booking";
    var API_UPDATE_PAYMENT_INFO = "/api_update_payment_info";


    //URL
    var URL_LOGIN = "/login";
    var URL_DASHBOARD = "/dashboard";
    var URL_SCHEDULE= "/schedule";
    var URL_SEC_INFO = "/secondary_info";


    //COOKIES
    var COOKIE_AUTH_TOKEN = "auth_token";

    //API CONFIG
    var TEACHER = "202";
    var STUDENT = "101";

    //MOCK IMG
    var URL_MOCK_COURSE = "";

    this.init = function () {

        hook_events();
    };

    this.payments = function (payment_hash) {
        schedule_a_booking(payment_hash);
    };

    var hook_events = function () {

        setup_ui();
        signup_js();
        login_js();
        show_password();
        logged_out_toast();
        session_logout();
        sec_info_js();
        remove_input_error();
        add_course();
        get_user_courses();
        search_courses();
        schedule_course();
    };

    var validator = function (id, text, type) {

        if (type == "email") {
            var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var result = pattern.test(text);
            if (!result) {
                $("#" + id).val('');
                $("#" + id).attr("placeholder", I18n.errors.e_email);
            }
            return result;
        }
        else if (type == "required") {
            if (text) {
                return true;
            } else {
                var old_placeholder = $("#" + id).attr("placeholder");
                $("#" + id).attr("placeholder", old_placeholder + I18n.errors.e_input_mand);
                return false;
            }
        }
        else if (type == "password") {
            if (text.length >= 6) {
                return true;
            } else {
                $("#" + id).val('');
                $("#" + id).attr("placeholder", I18n.errors.e_passwd);
                return false;
            }
        } else if (type == "mobile") {
            if (text.length == 10 && $.isNumeric(text)) {
                return true;
            } else {
                $("#" + id).val('');
                $("#" + id).attr("placeholder", I18n.errors.e_mobile);
                return false;
            }
        } else if (type == "number") {
            if ($.isNumeric(text)) {
                return true;
            } else {
                $("#" + id).val('');
                $("#" + id).attr("placeholder", I18n.errors.e_number);
                return false;
            }
        }


    };

    var show_input_error = function (id_hash) {
        var result = true;
        for (var id in id_hash) {
            var validity = validator(id, $("#" + id).val(), id_hash[id]);
            if (!validity) {
                $("#" + id).addClass("input_error");
                result = false;
            }
        }
        return result;
    };

    var remove_input_error = function () {
        $('input').on('keypress', function () {
            if ($(this).hasClass("input_error")) {
                $(this).removeClass("input_error");
            }
        });
    };

    var signup_js = function () {
        $('#btn-signup').on('click', function () {
            var validity_array = {
                "user_name": "required",
                "user_email": "required",
                "user_password": "required"
            };
            if (!show_input_error(validity_array)) {
                return;
            }
            var validity_array = {
                "user_password": "password",
                "user_email": "email"
            };
            if (!show_input_error(validity_array)) {
                return;
            }

            if (!validator("user_email", $('#user_email').val(), "email")) {
                show_toast(I18n.errors.e_email, "F");
                return;
            }
            var pdata = new FormData();
            pdata.append("name", $('#user_name').val());
            pdata.append("email", $('#user_email').val());
            pdata.append("password", $('#user_password').val());
            pdata.append("password_confirmation", $('#user_password').val());
            pdata.append("permission", $('input[name="p_role"]:checked').val());
            $.ajax({
                type: "POST",
                url: API_SIGNUP,
                data: pdata,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        show_toast(I18n.success.s_signup, "S");
                        redirect(URL_LOGIN);
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {

                }
            });
        });
    };

    var show_password = function () {
        $('#show_passw').on('click', function () {
            var field_type = $('#user_password').attr('type');
            if (field_type == 'text') {
                $('#user_password').attr('type', 'password');
            } else {
                $('#user_password').attr('type', 'text');
            }
        });
    };

    var login_js = function () {
        $('#btn-login').on('click', function () {
            var email = $('#user_email').val();
            var passwd = $('#user_password').val();

            var validity_array = {
                "user_email": "required",
                "user_password": "required"
            };
            if (!show_input_error(validity_array)) {
                return;
            }

            var validity_array = {
                "user_password": "password",
                "user_email": "email"
            };
            if (!show_input_error(validity_array)) {
                return;
            }
            var pdata = new FormData();
            pdata.append("email", email);
            pdata.append("password", passwd);
            $.ajax({
                type: "POST",
                url: API_LOGIN,
                data: pdata,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        delete_global_cookie(COOKIE_AUTH_TOKEN);
                        show_toast(I18n.success.s_login, "S");
                        store_global_cookie(COOKIE_AUTH_TOKEN, response["auth_token"]);
                        redirect(response['redirect_url']);
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {

                }
            });

        });
    };

    var sec_info_js = function () {

        $('#btn_sec_continue').on('click', function () {
            var role = $('#id_role').attr('data-role');
            var pdata = {};

            if (!role) {
                show_toast(I18n.errors.e_role, "F");
                return;
            }

            if (role.toString() == TEACHER) {
                pdata["static_info"] = {
                    "title": $('#usi_title').val(),
                    "qualif": $('#usi_qualif').val(),
                    "mobile": $('#usi_mobile').val(),
                    "exp": $('#usi_exp').val(),
                    "location": $('#usi_location').val(),
                    "bio": $('#usi_bio').val(),
                    "gender": $('input[name="usi_gender"]:checked').val()
                };
                var validity_array = {
                    "usi_title": "required",
                    "usi_qualif": "required",
                    "usi_mobile": "required",
                    "usi_bio": "required"
                };
                if (!show_input_error(validity_array)) {
                    return;
                }
                var validity_array = {
                    "usi_mobile": "mobile"
                };
                if (!show_input_error(validity_array)) {
                    return;
                }

            } else if (role.toString() == STUDENT) {
                pdata['standard'] = $('#usi_standard').val();
                pdata['location'] = $('#usi_location').val();
                pdata['mobile_number'] = $('#usi_mobile').val();
                var validity_array = {
                    "usi_mobile": "required"
                };
                if (!show_input_error(validity_array)) {
                    return;
                }
                var validity_array = {
                    "usi_mobile": "mobile"
                };
                if (!show_input_error(validity_array)) {
                    return;
                }
            }
            $.ajax({
                type: "POST",
                url: API_SECONDARY_INFO,
                data: pdata,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        show_toast(I18n.success.s_save, "S");
                        redirect(URL_DASHBOARD);
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {
                    show_loader(false);
                },
                beforeSend: function (xhr) {
                    show_loader(true);
                    set_auth_header(xhr)
                }
            });
        });
    };

    var add_course = function () {

        $('#btn-add-course').on('click', function () {

            var time_slots = $('#course_slots').val();

            var validity_array = {
                "course_name": "required",
                "course_subject": "required",
                "course_location": "required",
                "course_rph": "required",
                "course_rpm": "required"
            };
            if (!show_input_error(validity_array)) {
                return;
            }
            if (!time_slots) {
                show_toast(I18n.errors.e_slots, "F");
                return;
            }

            var validity_array = {
                "course_rph": "number",
                "course_rpm": "number"
            };
            if (!show_input_error(validity_array)) {
                return;
            }

            var pdata = {
                "c_name": $('#course_name').val(),
                "c_subject": $('#course_subject').val(),
                "c_location_addr": $('#course_location').val(),
                "c_slots": time_slots,
                "c_rates": {
                    "rph": $('#course_rph').val(),
                    "rpm": $('#course_rpm').val()
                },
                "c_desc": $('#course_desc').val(),
                "c_stud_limit": $('#course_limit').val(),
                "c_course_img": $('#course_thumb').attr('src')
            };

            $.ajax({
                type: "POST",
                url: API_ADD_COURSE,
                data: pdata,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        show_toast(I18n.success.s_add_course, "S");
                        clear_form('#form_add_course');
                        get_user_courses();
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {
                    show_loader(false);
                },
                beforeSend: function (xhr) {
                    show_loader(true);
                    set_auth_header(xhr)
                }
            });

        });

    };

    var get_user_courses = function () {

        if ($('#myCoursesTemplate').length > 0) {
            $.ajax({
                type: "GET",
                url: API_COURSES_USER,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        $('.div-results').html('');
                        var c_data = response["data"];
                        var c_data_len = c_data.length;
                        $('#user_c_count').text(c_data_len);
                        for (var i in c_data) {
                            if(!c_data[i]["c_course_img"]){
                                c_data[i]["c_course_img"] = URL_MOCK_COURSE;
                            }
                            c_data[i]["c_rates"] = JSON.parse(c_data[i]["c_rates"]);
                            c_data[i]["c_slots"] = JSON.parse(c_data[i]["c_slots"]);
                        }

                        $('#myCoursesTemplate').tmpl(c_data).appendTo('.div-results');
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {
                    show_loader(false);
                },
                beforeSend: function (xhr) {
                    show_loader(true);
                    set_auth_header(xhr)
                }
            });
        }
    };

    var schedule_a_booking = function (payment_hash) {

        $('#btn-go-back-schedule').on('click', function(){
            $('.schedule_info').show();
            $('.payment-info').hide();
        });

        $('#btn-schedule').on('click', function(){
            var pdata = {
                "booking_id": $('#exist_booking_id').val(),
                "sb_start_date": $('#schedule_date').val(),
                "sb_slot":$('input[name=opt_slots]:checked').val(),
                "sb_plan":$('input[name=opt_plan]:checked').val(),
                "course_id": $('#schedule_cid').val()
            };
            $.ajax({
                type: "POST",
                url: API_SCHEDULE_A_BOOKING,
                dataType: "json",
                data: pdata,
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        $('.schedule_info').hide();
                        $('.payment-info').show();
                        $('#exist_booking_id').val(response['data']['booking_id']);
                        $('#booking_id').text(response['data']['booking_id']);
                        $('#booking_duration').text(response['data']['booking_duration']);
                        $('#booking_st_date').text(response['data']['booking_st_date']);
                        $('#booking_time_slot').text(response['data']['booking_time_slot']);
                        $('#course_rate').text(response['data']['booking_payment']['course_rate']);
                        $('#course_total').text(response['data']['booking_payment']['course_total']);
                        $('#course_tax_per').text(response['data']['booking_payment']['course_tax_per']);
                        $('#course_tax_val').text(response['data']['booking_payment']['course_tax_val']);
                        $('#course_subtotal').text(response['data']['booking_payment']['course_total']);
                        $('#course_amount').text(response['data']['booking_payment']['course_amount']);
                        payment_hash['amount'] = response['data']['booking_payment']['course_amount'];
                        payment_hash['booking_id'] = response['data']['booking_id'];
                        payment_setup(payment_hash);
                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {
                    show_loader(false);
                },
                beforeSend: function (xhr) {
                    show_loader(true);
                    set_auth_header(xhr)
                }
            });
        });
    };

    var logged_out_toast = function () {
        var auth_key = $('#id_auth_key').attr('data-auth-key');
        if (auth_key == "N") {
            show_toast(I18n.errors.e_logged_out, "F");
        } else if (auth_key == "M") {
            show_toast(I18n.errors.e_access_denied, "F");
        }
    };

    var set_auth_header = function (xhr) {
        xhr.setRequestHeader("Authorization", read_global_cookie(COOKIE_AUTH_TOKEN));
    };

    var store_global_cookie = function (key, value) {
        document.cookie = key + "=" + value + ";path=/";
    };

    var delete_global_cookie = function (key) {
        document.cookie = key + "=;path=/";
    };

    var read_global_cookie = function (key) {
        var result;
        var web_cookie = document.cookie;
        web_cookie = web_cookie.split(";");
        for (var c in web_cookie) {
            var wc = web_cookie[c].split("=");
            if (wc[0] == key) {
                result = wc[1];
                break;
            }
        }
        return result;
    };

    var clear_form = function (id) {
        $(id).find('input[type=text]').val('');
        $(id).find('input[type=number]').val('');
        $(id).find('select').val('').trigger('change');
        $(id).find('textarea').val('');
    };

    var redirect = function (re_url) {
        setTimeout(function () {
            window.location = re_url;
        }, 2000);
    };

    var session_logout = function () {
        $('#btn-logout').on('click', function () {
            delete_global_cookie(COOKIE_AUTH_TOKEN);
        });
    };

    var show_toast = function (text, flag) {
        var toast = $('#snackbar');
        toast.removeClass();
        if (flag == "S") {
            toast.addClass('toast-success');
        } else if (flag == "F") {
            toast.addClass('toast-fail');
        }
        toast.text(text);
        toast.addClass('show');
        setTimeout(function () {
            $('#snackbar').removeClass('show');
        }, 3000);
    };

    var show_loader = function (flag) {
        if(flag){
            $('.load-bar').show();
        }else{
            $('.load-bar').hide();
        }
    };

    var fill_distance_matrix = function (origin_loc, destination_hash, id_salt) {

        if(Object.keys(destination_hash).length == 0 || !origin_loc){
            return;
        }
        var pdata = {};
        pdata['origin_loc'] = origin_loc;
        pdata['dest_hash'] = destination_hash;
        $.ajax({
            type: "POST",
            url: API_DISTANCE_MATRIX,
            data: pdata,
            dataType: "json",
            success: function (response) {
                if (response["errors"]) {
                    show_toast(response["errors"], "F");
                } else {
                    var dist_hash = response['data'];
                    for(var i in dist_hash){
                        var dist = dist_hash[i]['dist'];
                        if(!dist)
                            dist = "NA";
                        $('#'+id_salt+'_'+i).text(dist);
                    }
                }
            },
            error: function (response) {
                show_toast(I18n.errors.e_102, "F");
            },
            complete: function (response) {
                show_loader(false);
            },
            beforeSend: function (xhr) {
                show_loader(true);
                set_auth_header(xhr)
            }

        });
    };

    var search_courses = function(){
        $('#btn-search').on('click', function () {
            $('.div-results').html('');
            var pdata = {};
            pdata['q'] = $('#search_c_name').val();
            pdata['sub'] = $('#search_subject').val();
            if(!pdata['q'] && !pdata['sub']){
                show_toast(I18n.errors.e_empty_q, "F");
                return;
            }
            $.ajax({
                type: "GET",
                url: API_SEARCH_COURSES,
                data: pdata,
                dataType: "json",
                success: function (response) {
                    if (response["errors"]) {
                        show_toast(response["errors"], "F");
                    } else {
                        show_toast(I18n.success.s_result, "S");
                        $('.msg_layout').hide();
                        var c_data = response["data"];
                        if(c_data.length == 0){
                            show_toast(I18n.no_result, "S");
                        }
                        var c_teachers = response["teachers"];
                        var c_data_len = c_data.length;
                        $('#search-count').text(c_data_len);
                        var distance_hash = {};
                        for (var i in c_data) {
                            c_data[i]["c_rates"] = JSON.parse(c_data[i]["c_rates"]);
                            c_data[i]["c_slots"] = JSON.parse(c_data[i]["c_slots"]);
                            c_data[i]["c_teacher"] = c_teachers[c_data[i]["elt_users_id"]];
                            distance_hash[c_data[i]["id"]] =  c_data[i]["c_location_addr"];
                        }
                        var origin_loc = $('#search_location').val();
                        if(origin_loc){
                            fill_distance_matrix(origin_loc, distance_hash, "course_location");
                        }
                        $('#searchCoursesTemplate').tmpl(c_data).appendTo('.div-results');

                    }
                },
                error: function (response) {
                    show_toast(I18n.errors.e_102, "F");
                },
                complete: function (response) {
                    show_loader(false);
                },
                beforeSend: function (xhr) {
                    show_loader(true);
                    set_auth_header(xhr)
                }
            });
        });
    };

    var setup_ui = function () {
        $(document).ready(function () {

            $('#course_slots').select2({
                placeholder: I18n.placeholder.plc_time_slots
            });

            $('#course_subject').select2({
                placeholder: I18n.placeholder.plc_sub
            });

            $('#search_subject').select2({
                placeholder: I18n.placeholder.plc_sub
            });

            $('#sort_by').select2({
                placeholder: I18n.placeholder.plc_sort
            });

            $('#usi_standard').select2({
                placeholder: I18n.placeholder.plc_std
            });

            location_ui();
            uploader_ui();
        });
    };

    var payment_setup = function(payment_hash){
            var options = {
                "key": payment_hash["key"],
                "amount": payment_hash["amount"]*100,
                "name": "Study theatre",
                "description": payment_hash["desc"],
                "image": "",
                "handler": function (response){
                    var payment_id = response.razorpay_payment_id;
                    if(payment_id){
                        $('#schedule_box').hide();
                        $('.msg_layout').show();
                        $('#id_msg_sub_txt_1').text('Transaction ID: '+ payment_id);
                        var pdata = {
                            "id": payment_hash["booking_id"],
                            "pay_id": payment_id
                        };
                        $.ajax({
                            type: "POST",
                            url: API_UPDATE_PAYMENT_INFO,
                            data: pdata,
                            dataType: "json",
                            success: function (response) {
                                if (response["errors"]) {
                                    show_toast(response["errors"], "F");
                                } else {
                                    show_toast(I18n.success.s_payment, "S");
                                }
                            },
                            error: function (response) {
                                show_toast(I18n.errors.e_102, "F");
                            },
                            complete: function (response) {
                                show_loader(false);
                            },
                            beforeSend: function (xhr) {
                                show_loader(true);
                                set_auth_header(xhr)
                            }
                        });
                    }else{
                        show_toast(I18n.errors.e_102, "F");
                    }
                },
                "prefill": {
                    "name": payment_hash["name"],
                    "email": payment_hash["email"],
                    "contact": payment_hash["contact"]
                },
                "notes": {
                    "address": "Wonderland World"
                },
                "theme": {
                    "color": "#25A386"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-pay-button').onclick = function(e){
                rzp1.open();
                e.preventDefault();
            };
    };

    var uploader_ui = function(){

        $('#course_thumb').on('click', function (){
           $('#phantom_file_upload').trigger('click');
           $('#phantom_file_upload').attr('data-img-holder','course_thumb');
        });

        $('#phantom_file_upload').on("change", function(e) {
            var file = this.files[0];
            var type = file.type.split("/")[1];
            if(type != "png" && type != "jpg"){
                show_toast(I18n.errors.e_file_type, "F");
                return;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                var img_value = e.target.result ;
                var pdata = {};
                pdata["key"] = "sth_img_";
                pdata["img_data"] = img_value;
                $.ajax({
                    type: "POST",
                    url: API_IMAGE_UPLOAD,
                    data: pdata,
                    dataType: "json",
                    success: function (response) {
                        if (response["errors"]) {
                            show_toast(response["errors"], "F");
                        } else {
                            show_toast(I18n.success.s_save, "S");
                            $('#phantom_file_upload').val('');
                            var img_holder = $('#phantom_file_upload').attr('data-img-holder');
                            $('#'+img_holder).css('background-image','url('+response['data']+')');
                        }
                    },
                    error: function (response) {
                        show_toast(I18n.errors.e_102, "F");
                    },
                    complete: function (response) {
                        show_loader(false);
                    },
                    beforeSend: function (xhr) {
                        show_loader(true);
                        set_auth_header(xhr)
                    }

                });
            };
            reader.readAsDataURL(file);
        });
    };

    var schedule_course = function () {
        $(document.body).on('click', '.btn-schedule', function() {
            var cid = $(this).attr('data-cid');
            window.location = URL_SCHEDULE+"?cid="+cid;
        });
    };

    var location_ui = function () {
        $('.location-input').on("click", function () {
            $('#location_modal').modal({backdrop: 'static', keyboard: false});
        });

        $('#btn-add-location').on('click', function () {
            $('.location-input').val($('#map_autocomplete').val());
            $('#location_modal').modal('hide');
        });

        $('#btn-close-modal').on('click', function () {
            $('#location_modal').modal('hide');
        });

        $("#map_autocomplete").keyup(function (event) {
            if (event.keyCode == 13) {
                $('.location-input').val($('#map_autocomplete').val());
                $('#location_modal').modal('hide');
            }
            if (event.keyCode == 27) {
                $('#location_modal').modal('hide');
            }
        });

    };
};