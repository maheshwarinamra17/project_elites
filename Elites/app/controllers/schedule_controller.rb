class ScheduleController < ApplicationController

  before_action :authenticate_api_request

  def schedule_a_booking
    result = {}
    user_id =  @current_user['id']
    Rails.logger.error params
    if !params[:sb_start_date].present? or !params[:sb_slot].present?  or !params[:sb_plan].present? or !params[:course_id].present?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
      render :json => result and return
    end
    available_plans = APP_CONFIG['course_plan'].values
    course_plan = params[:sb_plan]
    if !available_plans.include?(course_plan.to_s)
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_course_plan')
      render :json => result and return
    end
    if user_id.present?
      params[:elt_courses_id] = params[:course_id]
      params[:elt_users_id] = user_id
      user_role = @current_user['permission']
      if user_role == APP_CONFIG['user_type']['student']
        booking_id = params[:booking_id]
        if booking_id.present?
          result['data'] = EltStudentBooking.edit_a_booking(params)
        else
          result['data'] = EltStudentBooking.schedule_a_booking(params)
        end
        if result['data'].nil?
          if (!result['errors'])
            result['errors'] = Array.new
          end
          result['errors'].push(I18n.t 'errors.e_103')
        elsif result['data'].blank?
          if (!result['errors'])
            result['errors'] = Array.new
          end
          result['errors'].push(I18n.t 'errors.e_empty_post')
        end
      else
        if (!result['errors'])
          result['errors'] = Array.new
        end
        result['errors'].push(I18n.t 'errors.e_access_denied')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_101')
    end
    render :json => result
  end

  def cancel_a_booking
    result = {}
    user_id =  @current_user['id']
    booking_id = params[:id]
    if !booking_id.present?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
      render :json => result and return
    end
    if user_id.present?
      result['data'] = EltStudentBooking.cancel_a_booking(user_id,booking_id)
      if result['data'].nil?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_103')
      elsif result['data'].blank?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_empty_post')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_101')
    end
    render :json => result
  end

  def update_payment_info
    result = {}
    pay_id =  params[:pay_id]
    booking_id = params[:id]
    if !booking_id.present?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
      render :json => result and return
    end
    if pay_id.present?
      result['data'] = EltStudentBooking.update_payment_info(pay_id, booking_id)
      if result['data'].nil?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_103')
      elsif result['data'].blank?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_empty_post')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_pay_id')
    end
    render :json => result
  end



end