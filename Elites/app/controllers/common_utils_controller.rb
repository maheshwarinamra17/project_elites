class CommonUtilsController < ApplicationController

  before_action :authenticate_api_request

  def get_distance_matrix
    result = {}
    origin_location = params[:origin_loc]
    destination_hash = params[:dest_hash]
    if !origin_location.present? and !destination_hash.present?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
      render :json => result and return
    end
    common_utils = CommonUtils.new
    result['data'] = common_utils.get_location_data origin_location, destination_hash
    if result['data'].nil?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_103')
    end
    render :json => result
  end

end