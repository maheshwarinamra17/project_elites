class ApplicationController < ActionController::Base

  # protect_from_forgery with: :exception
  ## Add the line below to any controller to check auth_user
  ## before_action :authenticate_request

  attr_reader :current_user
  helper_method :get_secondary_info, :translations_js

  private

  ## Donot rely on cookies for API
  ## because cookies are only for websites

  def authenticate_api_request
    auth_user = AuthenticateUser.new
    auth_user.headers = request.headers
    user = auth_user.get_authorized_user
    if(user['auth_user'].present?)
      @current_user = user['auth_user']
    else
      result = {}
      result['errors'] = Array.new()
      result['errors'] = user['errors']
      render json: result, status: 200
    end
  end

  def authenticate_page_request
    auth_token = cookies[:auth_token]
    if !auth_token.present?
      redirect_to "/login?auth=N" and return
    end
    auth_user = AuthenticateUser.new
    request.headers['Authorization'] = auth_token
    auth_user.headers = request.headers
    user = auth_user.get_authorized_user
    if(user['auth_user'].present?)
      @current_user = user['auth_user']
    else
      redirect_to "/login?auth=M"
    end
  end

  def get_secondary_info
    if @current_user.present?
      if(@current_user['permission'] == APP_CONFIG['user_type']['student'])
        @current_user_info = EltStudentAuxInfo.get_secondary_info(@current_user['id'])
      elsif (@current_user['permission'] == APP_CONFIG['user_type']['teacher'])
        @current_user_info = JSON.parse(EltTeacherAuxInfo.get_secondary_info(@current_user['id'])[:static_info])
      end
    end
  end

  def translations_js
    translations = I18n.t(".").with_indifferent_access
    return translations
  end

end
