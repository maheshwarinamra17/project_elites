class PageController < ApplicationController

  before_action :authenticate_page_request, except: [:home, :login_user, :signup, :tnc, :pricing, :privacy_policy, :contact_us]

  def login_user
    respond_to do |format|
      format.html { render :template => "basic/login" }
    end
  end

  def dashboard
    respond_to do |format|
      if (@current_user.present?)
        get_secondary_info
      end
      @subject_list = EltDropdown.get_values(APP_CONFIG['dropdowns']['subjects'])
      if (@current_user['permission'] == APP_CONFIG['user_type']['student'])
        @standard = EltDropdown.get_values(APP_CONFIG['dropdowns']['std'])
        format.html { render :template => "basic/student_dashboard" }
      elsif (@current_user['permission'] == APP_CONFIG['user_type']['teacher'])
        @slot_list = EltDropdown.get_values(APP_CONFIG['dropdowns']['slots'])
        format.html { render :template => "basic/teacher_dashboard" }
      end
    end
  end

  def schedule
    if (@current_user.present?)
      get_secondary_info
    end
    cid = params[:cid]
    if cid.present?
      respond_to do |format|
        @schedule_info = EltStudentBooking.get_schedule_info(cid)
        if @schedule_info['data'].present?
          @schedule_info['data']['c_rates'] = JSON.parse(@schedule_info['data']['c_rates'])
          format.html { render :template => "basic/schedule" }
        else
          format.html { render :template => "basic/404" }
        end
      end
    end
  end

  def home
    respond_to do |format|
      format.html { render :template => "basic/home" }
    end
  end

  def signup
    respond_to do |format|
      format.html { render :template => "basic/signup" }
    end
  end

  def pricing
    respond_to do |format|
      format.html { render :template => "basic/pricing" }
    end
  end

  def privacy_policy
    respond_to do |format|
      format.html { render :template => "basic/privacy_n_pricing_policy" }
    end
  end
  def tnc
    respond_to do |format|
      format.html { render :template => "basic/tnc" }
    end
  end

  def contact_us
    respond_to do |format|
      format.html { render :template => "basic/contact_us" }
    end
  end

  def secondary_info
    if (@current_user['permission'] == APP_CONFIG['user_type']['student'])
      @standard = EltDropdown.get_values(APP_CONFIG['dropdowns']['std'])
    end
    respond_to do |format|
      format.html { render :template => "basic/secondary_info" }
    end
  end

  def students
    if (@current_user.present?)
      get_secondary_info
    end
    cid = params[:c_id]
    @students = nil
    if cid.present?
      @students = EltStudentBooking.get_course_students(cid, @current_user['id'])
    end
    respond_to do |format|
      format.html { render :template => "basic/students" }
    end
  end

end