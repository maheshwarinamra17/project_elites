class UserController < ApplicationController

  before_action :authenticate_api_request, except: [:create_a_new_user]

  def create_a_new_user
    result = {}
    if params[:name].present? and params[:email].present? and  params[:password].present?
      params[:permission] = APP_CONFIG['user_type'][params[:permission]]
      if params[:permission].present?
        new_user = EltUser.create_a_new_user(params)
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_role')
      end
      if new_user.present? and new_user[:id].present?
        result['user_sign_up'] = true
        result['email'] = new_user[:email]
      else
        if(!result['errors'])
          result['errors'] = Array.new
        end
        result['errors'].push(I18n.t 'errors.e_reg_email')
      end
    else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_uname')
    end
    render :json => result and return
  end

  def create_secondary_info
    result = {}
    user_id =  @current_user['id']
    if user_id.present?
      params[:elt_users_id] = user_id
      user_role = @current_user['permission']
      if user_role == APP_CONFIG['user_type']['teacher']
        if !params[:static_info].present?
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_mand')
          render :json => result and return
        end
        t_info = EltTeacherAuxInfo.create_secondary_info(params)
        result['id'] = t_info
      elsif user_role == APP_CONFIG['user_type']['student']
        if !params[:location].present? or !params[:standard].present? or  !params[:mobile_number].present?
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_mand')
          render :json => result and return
        end
        s_info = EltStudentAuxInfo.create_secondary_info(params)
        result['id'] = s_info
      end
      if(result['id'].present?)
        EltUser.where(id:user_id).update("profile_status" => true)
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_101')
    end
    render :json => result and return
  end  

end