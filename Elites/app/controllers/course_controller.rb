class CourseController < ApplicationController

  before_action :authenticate_api_request

  def add_a_course
    result = {}
    user_id =  @current_user['id']
    if user_id.present?
      params[:elt_users_id] = user_id
      user_role = @current_user['permission']
      if user_role >= APP_CONFIG['user_type']['teacher']
        if !params[:c_name].present? or !params[:c_subject].present? or !params[:c_location_addr].present? or !params[:c_slots].present? or !params[:c_rates].present?
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_mand')
          render :json => result and return
        end
        new_course = EltCourse.create_a_new_course(params)
        if new_course.present? and new_course['id'].present?
          result['id'] = new_course['id']
        else
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_101')
        end
      else
        if (!result['errors'])
          result['errors'] = Array.new
        end
        result['errors'].push(I18n.t 'errors.e_access_denied')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_101')
    end
    render :json => result
  end

  def get_courses_by_user
    result = {}
    user_id =  @current_user['id']
    if user_id.present?
     result['data'] = EltCourse.get_all_courses_by_a_user user_id
     if result['data'].nil?
       result['errors'] = Array.new
       result['errors'].push(I18n.t 'errors.e_103')
     elsif result['data'].blank?
       result['errors'] = Array.new
       result['errors'].push(I18n.t 'errors.e_empty_get')
     end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_101')
    end
    render :json => result
  end

  def search_courses
    result = {}
    if(!params[:q].present? and !params[:sub])
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
      render :json => result and return
    end
    result['data'] = EltCourse.get_course_by_name(params[:q], params[:sub])
    result['teachers'] = {}
    if(result['data'].present?)
      teachers = EltUser.where(id: result['data'].pluck(:elt_users_id).uniq).select(:id, :name)
      teachers.each do|teacher|
        result['teachers'][teacher.id] = teacher.name
      end
    end
    if result['data'].nil? or result['teachers'].nil?
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_103')
    end
    render :json => result
  end

end