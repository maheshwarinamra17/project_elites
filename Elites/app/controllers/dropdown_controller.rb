class DropdownController < ApplicationController

  before_action :authenticate_api_request

  # TODO secure this with admin permissions

  #  ===== Subjects ========
  # ["english","history","civics", "geography","mathematics","general science",
  # "physics","biology","chemistry","environmental science","computer science",
  # "physical education"]
  # ========================

  def add_subjects
    result = {}
  	subject_array = params[:data]
    if(subject_array.kind_of?(Array))
      result['data'] = EltDropdown.set_values(subject_array, APP_CONFIG['dropdowns']['subjects'])
    else
      if(!result['errors'])
        result['errors'] = Array.new()
      end
      result['errors'].push(I18n.t 'errors.e_format')
    end
    render :json => result and return
  end

  #  ===== Time slots =====
  #   ["06:00 AM - 07:00 AM", "07:00 AM - 08:00 AM", "08:00 AM - 09:00 AM",
  #   "09:00 AM - 10:00 AM", "10:00 AM - 11:00 AM", "06:30 AM - 07:30 AM",
  #   "07:30 AM - 08:30 AM", "08:30 AM - 09:30 AM", "09:30 AM - 10:30 AM",
  #   "10:30 AM - 11:30 AM", "01:00 PM - 02:00 PM", "02:00 PM - 03:00 PM",
  #   "03:00 PM - 04:00 PM", "04:00 PM - 05:00 PM", "05:00 PM - 06:00 PM",
  #   "06:00 PM - 07:00 PM", "01:30 PM - 02:30 PM", "02:30 PM - 03:30 PM",
  #   "03:30 PM - 04:30 PM", "04:30 PM - 05:30 PM", "05:30 PM - 06:30 PM",
  #   "06:30 PM - 07:30 PM"]
  # ========================

  def add_time_slots
    result = {}
    subject_array = params[:data]
    if(subject_array.kind_of?(Array))
      result['data'] = EltDropdown.set_values(subject_array, APP_CONFIG['dropdowns']['slots'])
    else
      if(!result['errors'])
        result['errors'] = Array.new()
      end
      result['errors'].push(I18n.t 'errors.e_format')
    end
    render :json => result and return
  end

  #  ===== Standards =====
  # ["5th", "6th", "7th", "8th", "9th", "10th", "11th", "12th"]
  # ========================

  def add_student_standards
    result = {}
    subject_array = params[:data]
    if(subject_array.kind_of?(Array))
      result['data'] = EltDropdown.set_values(subject_array, APP_CONFIG['dropdowns']['std'])
    else
      if(!result['errors'])
        result['errors'] = Array.new()
      end
      result['errors'].push(I18n.t 'errors.e_format')
    end
    render :json => result and return
  end

  def upload_img_to_s3
    result = {}
    key = params[:key]
    img_data = params[:img_data]
    if key.present? and img_data.present?
      common_utils = CommonUtils.new
      result['data'] = common_utils.upload_img_s3(key, img_data)
      if result['data'].nil?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_101')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_mand')
    end
    render :json => result
  end

  def test_api
    common_utils = CommonUtils.new
    result = common_utils.send_sms(9391734766, "Rails sms api")
    render :json => result
  end

end