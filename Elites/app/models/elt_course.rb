class EltCourse < ApplicationRecord

  def self.create_a_new_course new_course_params
    new_course_params[:c_rates] = new_course_params[:c_rates].to_json
    new_course_params[:c_slots] = new_course_params[:c_slots].to_json
    new_course_params[:c_course_status] = APP_CONFIG['course_status']['active']
    result = EltCourse.create(new_course_params.permit(:elt_users_id, :c_name, :c_desc, :c_subject, :c_rates, :c_location_addr, :c_slots, :c_stud_limit, :c_course_status,:c_course_img))
  rescue Exception => e
    result = nil
  end

  ## Course name, subject and student limit are uneditable
  def self.edit_course_info course_params
    result = EltCourse.where(id: course_params[:id]).update(course_params.permit(:c_desc, :c_rates, :c_location_addr, :c_slots))
  rescue Exception => e
    result = nil
  end

  def self.remove_a_course user_id, course_id
    result = EltCourse.where(id: course_id, elt_users_id: user_id).destroy_all
  rescue Exception => e
    result = nil
  end

  def self.get_all_courses_by_a_user user_id
    result = EltCourse.where(elt_users_id: user_id).order(updated_at: :desc)
  rescue Exception => e
    result = nil
  end

  def self.get_course_by_name search_string, search_subject = nil
    if search_subject.present?
      result = EltCourse.where("c_name ILIKE ? and c_subject = ?", "%#{search_string}%", search_subject).limit(APP_CONFIG['query_limit']).offset(0)
    else
      result = EltCourse.where("c_name ILIKE ?", "%#{search_string}%").limit(APP_CONFIG['query_limit']).offset(0)
    end
  rescue Exception => e
    Rails.logger.error e
    result = nil
  end

end