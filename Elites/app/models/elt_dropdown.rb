class EltDropdown < ApplicationRecord

  def self.set_values value_array, set
    data = Array.new()
    value_array.each do |value|
      data.push({
          :set => set,
          :value => value
      })
    end
    result = EltDropdown.create(data)
    rescue Exception => e
      result = nil
  end

  def self.get_values set
    result = EltDropdown.where(set: set).pluck(:value)
    rescue Exception => e
      result = nil
  end
  
end
