class EltStudentBooking < ApplicationRecord

  def self.schedule_a_booking booking_params
    booking_params[:sb_schedule_status] = true
    course = EltCourse.where(id: booking_params[:elt_courses_id], c_course_status: APP_CONFIG['course_status']['active']).first
    if course.present?
      result = {}
      booking_params[:elt_teacher_id] = course[:elt_users_id]
      booking = EltStudentBooking.create(booking_params.permit(:sb_start_date, :sb_slot, :sb_plan, :sb_schedule_status, :elt_courses_id, :elt_users_id, :elt_teacher_id))
      result['booking_id'] = booking['id']
      result['booking_duration'] = booking['sb_plan']
      result['booking_st_date'] = booking['sb_start_date']
      result['booking_time_slot'] = booking['sb_slot']
      payments_helper =  PaymentsHelper.new
      result['booking_payment'] = payments_helper.generate_payment_details(course, booking['sb_plan'])
    else
      result = nil
    end
    result
  rescue Exception => e
    result = nil
  end

  def self.edit_a_booking booking_params
    result = {}
    booking_params[:sb_schedule_status] = true
    course = EltCourse.where(id: booking_params[:elt_courses_id], c_course_status: APP_CONFIG['course_status']['active']).first
    if course.present?
      booking = EltStudentBooking.where(id: booking_params[:booking_id]).update(booking_params.permit(:sb_start_date, :sb_slot, :sb_plan, :sb_schedule_status)).first
      result['booking_id'] = booking['id']
      result['booking_duration'] = booking['sb_plan']
      result['booking_st_date'] = booking['sb_start_date']
      result['booking_time_slot'] = booking['sb_slot']
      payments_helper =  PaymentsHelper.new
      result['booking_payment'] = payments_helper.generate_payment_details(course, booking['sb_plan'])
    else
      result = nil
    end
    result
  rescue Exception => e
    result = nil
  end

  def self.update_payment_info payment_id, booking_id
    result = {}
    booking = EltStudentBooking.where(id: booking_id).update(:sb_payment_status => APP_CONFIG['payment_status']['paid'], :sb_payment_id => payment_id).first
    result = booking.attributes.slice('id', 'sb_payment_status', 'sb_payment_id')
    result
  rescue Exception => e
    result = nil
  end


  def self.cancel_a_booking user_id, booking_id
    booking_params[:sb_schedule_status] = false
    booking_params[:sb_payment_status] = APP_CONFIG['payment_status']['return']
    result = EltStudentBooking.where(booking_id: booking_id,  elt_users_id: user_id).update(booking_params.permit(:sb_schedule_status, :sb_payment_status))
  rescue Exception => e
    result = nil
  end

  def self.get_schedule_info cid
    result = {}
    result['data'] = EltCourse.where(id:cid.to_i).first
    if(result['data'].present?)
      result['data']['c_slots'] = JSON.parse(result['data']['c_slots'])
      teacher_id = result['data'][:elt_users_id]
      teacher_info = EltTeacherAuxInfo.get_secondary_info(teacher_id)
      teacher_info[:static_info] = JSON.parse(teacher_info[:static_info])
      result['teacher_info'] = teacher_info
    end
    result.as_json
  rescue Exception => e
    result = nil
  end

  def self.get_course_students c_id, user_id
    result = {}
    result['data'] =  EltStudentBooking.where(elt_courses_id: c_id, elt_teacher_id: user_id)
    if(result['data'].present?)
      result['students'] = {}
      students = EltUser.where(id: result['data'].pluck(:elt_users_id).uniq).select(:id, :name)
      students.each do|student|
        result['students'][student.id] = student.name
      end
    end
    result.as_json
  rescue Exception => e
    result = nil
  end
end