class EltUser < ApplicationRecord

  has_secure_password
  validates :email, :uniqueness => {:case_sensitive => false}

  def self.create_a_new_user new_user_params
    result = EltUser.create(new_user_params.permit(:name, :email, :password, :password_confirmation, :permission))
    rescue Exception => e
      result = nil
  end
  
end
