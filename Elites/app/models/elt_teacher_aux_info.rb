# TODO -  Remove rating from teachers aux info

class EltTeacherAuxInfo < ApplicationRecord

  def self.create_secondary_info new_user_params
    new_user_params[:static_info] =  new_user_params[:static_info].to_json
    result = EltTeacherAuxInfo.where(elt_users_id: new_user_params[:elt_users_id]).first_or_create!().update(new_user_params.permit(:elt_users_id, :static_info))
  rescue Exception => e
    result = nil
  end

  def self.get_secondary_info user_id
    result = EltTeacherAuxInfo.where(elt_users_id: user_id).select(:avatar_url,:static_info).first
  rescue Exception => e
    result = nil
  end
end