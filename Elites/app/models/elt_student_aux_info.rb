class EltStudentAuxInfo < ApplicationRecord

  def self.create_secondary_info new_user_params
    result = EltStudentAuxInfo.where(elt_users_id: new_user_params[:elt_users_id]).first_or_create!().update(new_user_params.permit(:location, :mobile_number, :standard))
  rescue Exception => e
    result = nil
  end

  def self.get_secondary_info user_id
    result = EltStudentAuxInfo.where(elt_users_id: user_id).first
  rescue Exception => e
    result = nil
  end

end