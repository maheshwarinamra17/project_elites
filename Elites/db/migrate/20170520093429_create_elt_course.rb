class CreateEltCourse < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_courses do |t|
      t.string :c_name
      t.text :c_desc
      t.text :c_subject
      t.json :c_rates
      t.string :c_location_addr
      t.json :c_slots
      t.integer :c_stud_limit
      t.integer :c_stud_current
    end
  end
end
