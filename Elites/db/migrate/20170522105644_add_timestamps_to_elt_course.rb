class AddTimestampsToEltCourse < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_courses, :created_at, :datetime)
    add_column(:elt_courses, :updated_at, :datetime)
  end
end
