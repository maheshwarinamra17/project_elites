class CreateEltTeacherAuxInfo < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_teacher_aux_infos do |t|
      t.text :avatar_url
      t.float :rating
      t.float :price_per_hour
      t.json :static_info
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
