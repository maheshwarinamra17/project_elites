class AddReferenceToAuxInfo < ActiveRecord::Migration[5.0]
  def change
    add_reference :elt_teacher_aux_infos, :elt_users, index: true
    add_reference :elt_student_aux_infos, :elt_users, index: true
  end
end
