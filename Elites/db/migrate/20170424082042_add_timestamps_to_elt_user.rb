class AddTimestampsToEltUser < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_users, :created_at, :datetime)
    add_column(:elt_users, :updated_at, :datetime)
  end
end
