class AddCourseImgToEltCourse < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_courses, :c_course_img, :string, :default => nil)
  end
end
