class ChangePasswordToPasswordDigest < ActiveRecord::Migration[5.0]
  def change
    rename_column :elt_users, :password, :password_digest
  end
end
