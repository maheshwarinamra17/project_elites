class AddAttrToEltUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column(:elt_users, :status, :boolean)
    add_column(:elt_users, :permission, :integer)
  end
end
