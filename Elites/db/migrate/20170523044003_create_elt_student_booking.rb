class CreateEltStudentBooking < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_student_bookings do |t|
      t.date :sb_start_date
      t.string :sb_slot
      t.string :sb_plan
      t.boolean :sb_schedule_status
      t.integer :sb_payment_status
    end
    add_reference :elt_student_bookings, :elt_courses, index: true
    add_reference :elt_student_bookings, :elt_users, index: true
  end
end
