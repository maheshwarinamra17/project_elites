class ChangeStudLimitToCourseStatus < ActiveRecord::Migration[5.0]
  def change
  	rename_column :elt_courses, :c_stud_current, :c_course_status
  end
end
