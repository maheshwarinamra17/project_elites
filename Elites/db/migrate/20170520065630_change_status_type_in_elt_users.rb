class ChangeStatusTypeInEltUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :elt_users, :status, :boolean
    add_column(:elt_users, :profile_status, :boolean, :default => false)
    add_column(:elt_users, :email_status, :boolean, :default => false)
  end
end
