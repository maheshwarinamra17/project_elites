class AddTeachersIdToEltStudBookings < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_student_bookings, :elt_teacher_id, :integer)
  end
end
