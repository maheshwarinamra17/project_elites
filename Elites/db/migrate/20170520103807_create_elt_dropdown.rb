class CreateEltDropdown < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_dropdowns do |t|
      t.string :set
      t.string :value
    end
  end
end
