class AddMoreInfoToStudentInfo < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_student_aux_infos, :location, :text)
    add_column(:elt_student_aux_infos, :mobile_number, :text)
  end
end
