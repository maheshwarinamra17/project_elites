class AddEltUserIdToEltCourse < ActiveRecord::Migration[5.0]
  def change
    add_reference :elt_courses, :elt_users, index: true
  end
end
