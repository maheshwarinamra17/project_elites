class AddTimeStampsToStudentInfo < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_student_aux_infos, :created_at, :datetime)
    add_column(:elt_student_aux_infos, :updated_at, :datetime)
  end
end
