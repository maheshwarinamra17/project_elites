class AddPayIdToEltStudentBookings < ActiveRecord::Migration[5.0]
  def change
    add_column(:elt_student_bookings, :sb_payment_id, :string, :default => nil)
  end
end
