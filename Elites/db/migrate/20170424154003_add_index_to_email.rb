class AddIndexToEmail < ActiveRecord::Migration[5.0]
  def change
    add_index :elt_users, [:email], :unique => true
  end
end
