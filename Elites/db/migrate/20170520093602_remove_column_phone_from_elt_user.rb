class RemoveColumnPhoneFromEltUser < ActiveRecord::Migration[5.0]
  def change
    remove_column :elt_users, :phone, :string
  end
end
