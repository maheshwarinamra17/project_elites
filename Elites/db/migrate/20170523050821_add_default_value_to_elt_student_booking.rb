class AddDefaultValueToEltStudentBooking < ActiveRecord::Migration[5.0]
  def change
    change_column :elt_student_bookings, :sb_schedule_status, :boolean, :default => false
    change_column :elt_student_bookings, :sb_payment_status, :integer, :default => 0
  end
end
