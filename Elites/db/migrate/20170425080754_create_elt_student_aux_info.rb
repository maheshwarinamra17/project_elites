class CreateEltStudentAuxInfo < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_student_aux_infos do |t|
      t.text :avatar_url
      t.string :standard
    end
  end
end
