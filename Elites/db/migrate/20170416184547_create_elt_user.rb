class CreateEltUser < ActiveRecord::Migration[5.0]
  def change
    create_table :elt_users do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :password
    end
  end
end
