
// Postgres Setup MAC
psql template1 -U apple (I changed the default username to my name)
CREATE ROLE namra superuser;
CREATE USER root WITH PASSWORD 'root';
CREATE DATABASE Elites_development;
GRANT ALL PRIVILEGES ON DATABASE Elites_development to root;
ALTER DATABASE Elites_development OWNER TO root;
